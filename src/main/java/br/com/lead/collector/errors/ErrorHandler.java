package br.com.lead.collector.errors;

import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;

@ControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
    @ResponseBody
    public ErrorMenssage handleValidationError(MethodArgumentNotValidException exeception){
        HashMap<String, ErrorObject> erros = new HashMap<>();

        BindingResult result = exeception.getBindingResult();

        
        for(FieldError violation : result.getFieldErrors()){
            erros.put(violation.getField(), new ErrorObject(violation.getDefaultMessage(), violation.getRejectedValue().toString()));
        }
        ErrorMenssage error = new ErrorMenssage(422, exeception.getBindingResult().getObjectName(), "Campos invalidos", erros);
        return error;
    }
}
class ErrorMenssage {
    private int errorCode;
    private String error;
    private String errorMessage;
    private HashMap<String, ErrorObject> fieldErrors;

    public ErrorMenssage(int errorCode, String error, String errorMessage, HashMap<String, ErrorObject> fieldErrors) {
        this.errorCode = errorCode;
        this.error = error;
        this.errorMessage = errorMessage;
        this.fieldErrors = fieldErrors;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public HashMap<String, ErrorObject> getFieldErrors() {
        return fieldErrors;
    }

    public void setFieldErrors(HashMap<String, ErrorObject> fieldErrors) {
        this.fieldErrors = fieldErrors;
    }
}

class ErrorObject{
    private String defaultMessage;
    private String rejectValue;

    public ErrorObject(String defaultMessage, String rejectValue) {
        this.defaultMessage = defaultMessage;
        this.rejectValue = rejectValue;
    }

    public String getDefaultMessage() {
        return defaultMessage;
    }

    public void setDefaultMessage(String defaultMessage) {
        this.defaultMessage = defaultMessage;
    }

    public String getRejectValue() {
        return rejectValue;
    }

    public void setRejectValue(String rejectValue) {
        this.rejectValue = rejectValue;
    }
}